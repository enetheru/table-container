@tool
class_name Cell extends Control

@export var coord : Vector2i :
	set( value ):
		coord = value
		cell_hash = hash( coord )
		name = "%s" % cell_hash

@onready var cell_hash : int = hash( coord )

var data

func has_text() -> bool:
	var child: Node = get_child(0)
	if child:
		if 'text' in child:
			return true
	return false

func set_text( text ):
	var child: Node = get_child(0)
	if child:
		if 'text' in child:
			child.text = text

func get_text() -> String:
	var child: Node = get_child(0)
	if child:
		if 'text' in child:
			return child.text
	return ""

# Sort those from top left to bottom right
static func sort_position( a : Cell, b : Cell ) -> bool:
	if a.coord.y < b.coord.y: return true
	if a.coord.y == b.coord.y and a.coord.x < b.coord.x: return true
	return false
