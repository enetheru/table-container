@tool
extends BoxContainer

var plugin

func set_headers():
	var table : Table = plugin.table
	var column_titles: PackedStringArray = ["ID", "Authority", "Clients",
	"Label", "Type", "Links", "NumObjects", "IsSaved", "IsActive", "InWorld"]
	table.write_array( column_titles, Vector2.ZERO, table.col_headers )

func generate_func():
	var table : Table = plugin.table
	var column_titles: PackedStringArray = ["ID", "Authority", "Clients",
	"Label", "Type", "Links", "NumObjects", "IsSaved", "IsActive", "InWorld"]
	table.write_array( column_titles, table.next_row() )

func clear_table():
	var table : Table = plugin.table
	plugin.table.clear()


func print_row():
	var table: Table        =  plugin.table
	var domain: Rect2i      =  table.final_domain
	var selection: Vector2i =  plugin.selected_cell
	var coord              := Vector2( 0, selection.y )
	var size               := Vector2( domain.size.x, 1 )
	var area               := Rect2i( coord, size )
	var cells: Array        =  table.find(func(c: Cell): return area.has_point(c.coord) )
	for cell in cells:
		if not cell: continue
		if not cell.get_child_count(): continue
		var first = cell.get_child(0)
		if first and first.has_method("get_text"):
			print( first.get_text() )

func print_info():
	print( "\nprint_info():")
	var table: Table = plugin.table
	print("Table Size: ", table.size )
	print("Cells Size: ", table.cells.size)
	print( "Max Ratio: ", table.ratio_max)
	print( "Domain: ", table.final_domain )
	print( "Widths: ", table.widths )
	print( "XOffsets: ", table.xoffset)

	print( "Heights: ", table.heights )
	print( "YOffsets: ", table.yoffset)

	print( "Total Min: ", table.total_min )
	print( "Total Max: ", table.total_max )
	var remaining: Vector2 = table.cells.size - table.total_min
	print( "Remainining: ", remaining )

	for header : Header in table.col_headers.get_children():
		print( "\t** Header: ", header.get_text() )
		print( "\tSize: ", header.size )
		print( "\tMin Size: ", header.get_combined_minimum_size() )
		print( "\tMax Size: ", header.maximum_size )
		print( "\tRatio: ", header.ratio )
		print( "\tNorm Ratio: ", header.ratio / table.ratio_max)
		print( "\tFinal Width: ", header.get_combined_minimum_size() + remaining / header.ratio )

func redo_domain():
	plugin.table.dirty_domain = true
	plugin.table.primary_layout()
	plugin.table.resized.emit()

func fit_content():
	plugin.selected_cell
	var header : Header = plugin.get_selected_cell()
	if header and header is Header:
		header.maximum_size = header.get_combined_minimum_size()
		plugin.table.resized.emit()

func auto_width():
	plugin.selected_cell
	var header : Header = plugin.get_selected_cell()
	if header and header is Header:
		header.maximum_size = Vector2.ZERO
		plugin.table.resized.emit()
