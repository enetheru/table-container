@tool
extends TextEdit

var plugin
var table : Table
var region : Control
var cell : Cell
var cell_coord : Vector2


func edit( _region : Control, _cell_coord : Vector2 ) -> void:
	clear()
	region = _region
	cell_coord = _cell_coord

	cell = region.get_node_or_null("%s" % hash( cell_coord ))

	if cell:
		if cell.has_text():
			text = cell.get_text()
			select_all()
		else:
			print( "The cell at this location doesnt have a text property")
			return
	show()
	grab_focus()


func close():
	clear()
	text = ""
	hide()
	# NOTE: This is a hack to return focus back to the 2d viewport
	Input.warp_mouse( plugin.last_mm.global_position )


func _gui_input(event: InputEvent) -> void:
	if event is InputEventKey:
		match [event.keycode, event.pressed]:
			[KEY_TAB, true]:
				table.write_cell( text, cell_coord, region )
				close()
				if event.shift_pressed:
					plugin.selected_cell += Vector2i(-1,0)
				else:
					plugin.selected_cell += Vector2i(1,0)
			[KEY_ESCAPE, true]:
				close()
			[KEY_ENTER, true]:
				if event.is_command_or_control_pressed():
					insert_text_at_caret('\n')
				else:
					table.write_cell( text, cell_coord, region )
					close()
					if event.shift_pressed:
						plugin.selected_cell += Vector2i(0,-1)
					else:
						plugin.selected_cell += Vector2i(0,1)
