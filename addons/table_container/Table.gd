@tool
class_name Table extends Control

const cell_script = preload('res://addons/table_container/Cell.gd')
const header_script := preload('res://addons/table_container/Header.gd')


#region Children
var col_headers : Control
var row_headers : Control
var cells: Control
#endregion

@export_group("Visual Domain")
@export var min_cols : int = 3 : set = set_min_cols

@export var min_rows : int = 3 : set = set_min_rows

@export var auto_domain : bool = true :
	set( value ):
		auto_domain = value
		resized.emit()

@export var domain_include_zero : bool = true :
	set( value ):
		domain_include_zero = value
		resized.emit()

@export var pre_domain := Rect2i(0,0,0,0) :
	get = get_pre_domain


@export_group("Columns")

@export var min_col_width : float = 100 :
	set( value ):
		min_col_width = value
		resized.emit()

@export var show_col_header : bool = true :
	set( value ):
		show_col_header = value
		# FIXME col_headers doesnt exist at the time this value is loaded
		# causing a invalid assignment error. We need to defer this to
		# some later time so that the value can be loaded from the scene
		col_headers.visible = value
		#resized.emit() # FIXME find out why this crashes when disabling column headers
		queue_redraw()

@export var col_header_height : float = 20

@export var auto_column_headers : bool = false : set = set_auto_column_headers

@export_group( "Rows")

@export var min_row_height : float = 20 :
	set( value ):
		min_row_height = value
		resized.emit()

@export var show_row_header : bool = true :
	set( value ):
		show_row_header = value
		if is_node_ready():
			row_headers.visible = value
			resized.emit()
			queue_redraw()

@export var row_header_width : float = 20

@export var auto_row_headers : bool = false : set = set_auto_row_headers

@export_group("Style")

@export var show_background : bool = true :
	set( value ):
		show_background = value
		queue_redraw()

# TODO add theme options like colours
# Alternating Colours
# header background colours
const COLOR_BASE := Color.WEB_GRAY * Color(1,1,1,0.2)
const COLOR_BASE_ALT := Color.DIM_GRAY * Color(1,1,1,0.2)

@export_group("Edit Tools")
@export var editor_controls : bool = true :
	set( value ):
		editor_controls = value
		queue_redraw()



#region Volatile Properties
var default_font
var default_font_size

var dirty_domain := true
var final_domain := Rect2i()

var dirty_range := true
var table_range := Rect2i() :
	get = get_range

# sizes
var corner_size : Vector2
var ratio_max : Vector2
var total_min : Vector2
var total_max : Vector2
var unbounded_width : bool = false
var unbounded_height : bool = false

var xoffset : Array[float] = []
var widths : Array[float] = []
var yoffset : Array[float] = []
var heights : Array[float] = []
#endregion


#region Lifetime
func _ready() -> void:
	default_font = ThemeDB.fallback_font
	default_font_size = ThemeDB.fallback_font_size

	resized.connect( _on_resized )
	# Make sure we have the requisite cell groups
	col_headers = get_node_or_null( "ColumnHeaders" )
	if not col_headers:
		print( "Unable to find 'ColumnHeaders': creating")
		col_headers = Control.new()
		col_headers.name = "ColumnHeaders"
		add_child( col_headers )
		col_headers.owner = get_tree().edited_scene_root

	row_headers = get_node_or_null( "RowHeaders" )
	if not row_headers:
		print( "Unable to find 'RowHeaders': creating")
		row_headers = Control.new()
		row_headers.name = "RowHeaders"
		add_child( row_headers )
		row_headers.owner = get_tree().edited_scene_root

	cells = get_node_or_null( "Cells" )
	if not cells:
		print( "Unable to find 'Cells': creating")
		cells = Control.new()
		cells.name = "Cells"
		add_child( cells )
		#if editor
		cells.owner = get_tree().edited_scene_root

	cells.child_entered_tree.connect( _on_child_enter )
	cells.child_exiting_tree.connect( _on_child_exit )
	row_headers.child_entered_tree.connect( _on_child_enter )
	row_headers.child_exiting_tree.connect( _on_child_exit )
	col_headers.child_entered_tree.connect( _on_child_enter )
	col_headers.child_exiting_tree.connect( _on_child_exit )
	_on_resized()
#endregion

#region Setters and Getters for properties with side effects
func set_min_cols( value ):
	min_cols = value
	dirty_domain = true
	_on_resized()

func set_min_rows( value ):
	min_rows = value
	dirty_domain = true
	resized.emit()

func set_auto_column_headers( value : bool ):
	auto_column_headers = value
	queue_redraw()

func set_auto_row_headers( value : bool ):
	auto_row_headers = value
	queue_redraw()

# The domain is the coordinate area that is displayed.
func get_pre_domain() -> Rect2i:
	if not is_node_ready(): return Rect2i()
	if auto_domain:
		var domain_begin := Vector2i.MAX
		var domain_end := Vector2i.MIN
		for cell : Cell in cells.get_children():
			domain_begin.x = mini( domain_begin.x, cell.coord.x )
			domain_begin.y = mini( domain_begin.y, cell.coord.y )
			domain_end.x = maxi( domain_end.x, cell.coord.x +1 )
			domain_end.y = maxi( domain_end.y, cell.coord.y +1 )
		for cell in col_headers.get_children():
			domain_begin.x = mini( domain_begin.x, cell.coord.x )
			domain_end.x = maxi( domain_end.x, cell.coord.x +1 )
		for cell in row_headers.get_children():
			domain_begin.y = mini( domain_begin.y, cell.coord.y )
			domain_end.y = maxi( domain_end.y, cell.coord.y +1 )

		if domain_begin == Vector2i.MAX:
			domain_begin = Vector2i.ZERO
			domain_end  = domain_begin

		if domain_include_zero:
			domain_begin.x = mini( domain_begin.x, 0 )
			domain_begin.y = mini( domain_begin.y, 0 )
			domain_end.x = maxi( domain_end.x, 0+1 )
			domain_end.y = maxi( domain_end.y, 0+1 )

		domain_end.x = maxi( domain_end.x, domain_begin.x + min_cols )
		domain_end.y = maxi( domain_end.y, domain_begin.y + min_rows )

		pre_domain = Rect2i( domain_begin, domain_end - domain_begin )

	return pre_domain

# The range is a coordinate area that encompasses all cells within the table.
func get_range() -> Rect2i:
	if not dirty_range: return table_range

	var range_begin := Vector2i.MAX
	var range_end := Vector2i.MIN
	for cell : Cell in cells.get_children():
		range_begin.x = mini( range_begin.x, cell.coord.x )
		range_begin.y = mini( range_begin.y, cell.coord.y )
		range_end.x = maxi( range_end.x, cell.coord.x +1 )
		range_end.y = maxi( range_end.y, cell.coord.y +1)

	if range_begin == Vector2i.MAX:
		range_begin = Vector2i.ZERO
		range_end  = Vector2i.ZERO

	table_range = Rect2( range_begin, range_end - range_begin )
	dirty_range = false
	return  table_range

#endregion

func is_coord_on_boundary( coord : Vector2i, area : Rect2 ) -> bool:
	if coord.x ==  table_range.position.x:
		return true
	elif coord.y ==  table_range.position.y:
		return true
	elif coord.x ==  table_range.position.x +  table_range.size.x -1:
		return true
	elif coord.y ==  table_range.position.y +  table_range.size.y -1:
		return true
	return false

#region local mouse position to cell coordinates

func pos2coord_x( position : Vector2 ) -> int:
	for i in range( final_domain.size.x ):
		var x = final_domain.position.x + i
		if position.x > xoffset[i] \
			and position.x < xoffset[i] + widths[i]:
				return x
	return Vector2i.MAX.x

func pos2coord_y( position : Vector2 ) -> int:
	for i in range( final_domain.size.y ):
		var y = final_domain.position.y + i
		if position.y > yoffset[i] \
			and position.y < yoffset[i] + heights[i]:
				return y
	return Vector2i.MAX.y

func pos2coord( position : Vector2 ) -> Vector2i:
	return Vector2i( pos2coord_x( position ), pos2coord_y( position ) )
#endregion


#region Get Rect areas for coordinates
func get_column_rect( coord : Vector2i ) -> Rect2:
	var coord_norm: Vector2i = coord - final_domain.position
	if not final_domain.has_point( coord ): return Rect2()
	return Rect2(
			cells.position.x + xoffset[coord_norm.x], 0,
			widths[coord_norm.x],size.y )

func get_row_rect( coord : Vector2i ) -> Rect2:
	var coord_norm: Vector2i = coord - final_domain.position
	if not final_domain.has_point( coord ): return Rect2()
	return Rect2(0,cells.position.y + yoffset[coord_norm.y],size.x, heights[coord_norm.y])

func get_row_heading_rect( coord : Vector2i ) -> Rect2:
	var rect := Rect2()
	rect.position.x = 0
	rect.position.y = yoffset[coord.y]
	rect.size.x = row_headers.size.x
	rect.size.y = heights[coord.y]
	return rect

func get_col_heading_rect( coord : Vector2i ) -> Rect2:
	var rect                 := Rect2()
	var norm_coord: Vector2i =  coord - final_domain.position
	if coord.x >= (final_domain.position.x + final_domain.size.x):
		rect.position.x = col_headers.size.x + ( coord.x - final_domain.size.x) * min_col_width
		rect.size.x = min_col_width
	elif coord.x < final_domain.position.x:
		rect.position.x = coord.x * min_col_width
		rect.size.x = min_col_width
	else:
		rect.position.x = xoffset[norm_coord.x]
		rect.size.x = widths[norm_coord.x]

	rect.position.y = 0
	rect.size.y = col_headers.size.y
	return rect

func get_cell_rect( coord : Vector2i ) -> Rect2:
	var rect := Rect2()

	var norm_coord: Vector2i = coord - final_domain.position

	# FIXME, this doesnt quite calculate the appropriate rect of the cell outside of the boundary
	if not final_domain.has_point( coord ):
		if coord.x >= (final_domain.position.x + final_domain.size.x):
			rect.position.x = cells.size.x + (coord.x - final_domain.size.x) * min_col_width
		else:
			rect.position.x = (coord.x - final_domain.position.x) * min_col_width
		if coord.y >= final_domain.position.y + final_domain.size.y:
			rect.position.y = cells.size.y + (coord.y - final_domain.size.y)* min_row_height
		else:
			rect.position.y = (coord.y - final_domain.position.y) * min_row_height

		rect.size = Vector2( min_col_width, min_row_height )
		return rect

	rect.position.x = xoffset[norm_coord.x]
	rect.position.y = yoffset[norm_coord.y]
	rect.size.x = widths[norm_coord.x]
	rect.size.y = heights[norm_coord.y]
	return rect
#endregion


#region Event Handling
func _draw() -> void:
	if not show_background: return

	for i in range( final_domain.size.x ):
		var x = final_domain.position.x + i
		for j in range( final_domain.size.y):
			var y                = final_domain.position.y + j
			var cell_rect: Rect2 = shrink_rect( get_cell_rect( Vector2(x,y) ) )
			cell_rect.position += cells.position
			draw_rect( cell_rect, COLOR_BASE if j % 2 else COLOR_BASE_ALT, true )

	#Auto Column Headers
	if show_col_header and auto_column_headers:
		var headers: Array[Header] = get_domain_column_headers()
		for i in range( headers.size() ):
			if headers[i]: continue
			var x                  = final_domain.position.x + i
			var column_rect: Rect2 = get_col_heading_rect( Vector2(x, 0) )
			column_rect.position += col_headers.position
			draw_rect( column_rect, COLOR_BASE, true )
			#TODO Figure out a translation for decimal to base 26 to use the alphabet for numbering.
			var string_size = default_font.get_string_size( "%s" % x, HORIZONTAL_ALIGNMENT_LEFT, -1, default_font_size )
			var string_position = column_rect.position + column_rect.size / 2 - string_size / 2 + Vector2(0, column_rect.size.y )
			draw_string( default_font, string_position, "%s" % x, HORIZONTAL_ALIGNMENT_LEFT, -1, default_font_size)

	# Auto Row headers
	if show_row_header and auto_row_headers:
		var headers: Array = get_domain_row_headers()
		for i in range( headers.size() ):
			if headers[i]: continue
			var y               = final_domain.position.y + i
			var row_rect: Rect2 = get_row_heading_rect( Vector2(0, y) )
			row_rect.position += row_headers.position
			draw_rect( row_rect, COLOR_BASE, true )
			#TODO Figure out a translation for decimal to base 26 to use the alphabet for numbering.
			var string_size = default_font.get_string_size( "%s" % y, HORIZONTAL_ALIGNMENT_LEFT, -1, default_font_size )
			var string_position = row_rect.position + row_rect.size / 2 - string_size / 2 + Vector2(0, row_rect.size.y )
			draw_string( default_font, string_position, "%s" % y, HORIZONTAL_ALIGNMENT_LEFT, -1, default_font_size)

func _on_child_enter( child : Cell ):
	if not final_domain.has_point( child.coord ):
		dirty_domain = true
	if not  table_range.has_point( child.coord ):
		dirty_range = true
	if dirty_domain or dirty_range:
		resized.emit()

func _on_child_exit( child : Cell ):
	if is_coord_on_boundary( child.coord,  table_range ):
		dirty_range = true
		await get_tree().process_frame # Hack to resize after child is free
	elif is_coord_on_boundary( child.coord, final_domain ):
		dirty_domain = true
		await get_tree().process_frame # Hack to resize after child is free
	if dirty_domain or dirty_range:
		resized.emit()


func _on_resized() -> void:
	if not is_node_ready(): return
	#NOTE: notifying the parent to resize triggers a layout change which is needed to shrink the total columns when possible.
	# This works when the parent has control of the layout, but I think it might fail when we have control of ourselves.
	get_parent_control().notification( NOTIFICATION_RESIZED )

	queue_redraw()
	dirty_domain = true

	# Set primary layout of objects based on column and row headers.
	primary_layout()

	# Finalise the domain based on the layout.
	finalise_domain()

	# We might need to re-do the layout depending on the domain.
	var layout_redo : bool = false
	if total_min.x > size.x:
		size.x = total_min.x + corner_size.x
		layout_redo = true
	if total_min.y > size.y:
		size.y = total_min.y + corner_size.y
		layout_redo = true
	if layout_redo:
		resized.emit()
		return

	#TODO Split resize_headers() into cols and rows so they can be conditionally run.
	resize_headers()
	resize_cells()

func primary_layout() -> void:
	if not is_node_ready(): return
	corner_size = Vector2( row_header_width, col_header_height )

	for cell in col_headers.get_children():
		corner_size.y = max( corner_size.y, cell.size.y )

	for cell in row_headers.get_children():
		corner_size.x = max( corner_size.x, cell.size.x )

	if not show_row_header: corner_size.x = 0
	if not show_col_header: corner_size.y = 0

	col_headers.position = Vector2( corner_size.x, 0 )
	col_headers.size = Vector2(size.x - corner_size.x, corner_size.y)

	row_headers.position = Vector2( 0, corner_size.y )
	row_headers.size = Vector2(corner_size.x, size.y - corner_size.y)

	cells.position = Vector2.ZERO
	cells.size = size
	if show_col_header:
		cells.position.y += corner_size.y
		cells.size.y -= corner_size.y

	if show_row_header:
		cells.position.x += corner_size.x
		cells.size.x -= corner_size.x

func finalise_domain():
	# reset final domain and limit values for re-calculation
	final_domain = pre_domain
	final_domain.size = Vector2.ZERO
	total_min = Vector2.ZERO
	total_max = Vector2.ZERO
	ratio_max = Vector2.ZERO
	unbounded_width = false
	unbounded_height = false

	# Lets start with the column headers.
	var c_headers: Array[Node] = col_headers.get_children()
	c_headers.sort_custom( func(a,b) : return a.coord.x < b.coord.x )
	var i : int = 0
	var c_header : Header = c_headers[i] as Cell if i < c_headers.size() else null
	var x: int            = final_domain.position.x
	while true:
		final_domain.size.x += 1
		if c_header and c_header.coord.x == x:
			if c_header.maximum_size.x > 0 \
				and c_header.size.x >= c_header.maximum_size.x:
					total_min.x += c_header.maximum_size.x
					ratio_max.x += 0
			else:
				total_min.x += c_header.get_combined_minimum_size().x
				ratio_max.x += c_header.ratio.x
			total_max.x += c_header.maximum_size.x

			if c_header.maximum_size.x <= 0: unbounded_width = true
			i += 1
			c_header = c_headers[i] as Header if i < c_headers.size() else null
		else:
			ratio_max.x += 0
			total_min.x += min_col_width
			total_max.x += min_col_width

		# Loop Limits
		if final_domain.size.x < min_cols: x += 1; continue
		if total_min.x >= cells.size.x: break
		if final_domain.size.x >= pre_domain.size.x:
			if unbounded_width or total_max.x > cells.size.x: break
		# else continue iterating.
		x += 1
		continue

	# Now lets do the row headers.
	var r_headers: Array[Node] = row_headers.get_children()
	r_headers.sort_custom( func(a,b) : return a.coord.y < b.coord.y )
	i = 0
	var r_header := r_headers[i] as Header if i < r_headers.size() else null
	var y: int   =  final_domain.position.y
	while true:
		final_domain.size.y += 1
		if r_header and r_header.coord.y == y:
			if r_header.maximum_size.y > 0 \
				and r_header.size.y >= r_header.maximum_size.y:
					total_min.y += r_header.maximum_size.y
					ratio_max.y += 0
			else:
				total_min.y += r_header.get_combined_minimum_size().y
				ratio_max.y += r_header.ratio.y
			total_max.y += r_header.maximum_size.y

			if r_header.maximum_size.y <= 0: unbounded_height = true
			i += 1
			r_header = r_headers[i] as Header if i < r_headers.size() else null
		else:
			ratio_max.y += 0
			total_min.y += min_row_height
			total_max.y += min_row_height

		# Loop Limits
		if final_domain.size.y < min_rows: y += 1; continue
		if total_min.y >= cells.size.y: break
		if final_domain.size.y >= pre_domain.size.y:
			if unbounded_height or total_max.y > cells.size.y: break
		# else continue iterating.
		y += 1
		continue


func resize_headers():
	var remaining_size : Vector2 = cells.size - total_min
	if remaining_size.x < 0: remaining_size.x = 0

	widths.resize( final_domain.size.x )
	xoffset.resize( final_domain.size.x )
	heights.resize( final_domain.size.y )
	yoffset.resize( final_domain.size.y )


	var c_headers: Array[Node] = col_headers.get_children()
	c_headers.sort_custom( func(a,b) : return a.coord.x < b.coord.x )
	var i : int = 0
	var c_header : Header = c_headers[i] as Cell if i < c_headers.size() else null


	# Loop through column headers to find width and x position
	var current_position := Vector2.ZERO
	for j in range( final_domain.size.x ):
		var x = final_domain.position.x + j
		if c_header and c_header.coord.x == x:
			# There exists a header at this x coord.
			var min_width: float  = c_header.get_combined_minimum_size().x
			var norm_ratio: float = c_header.ratio.x / ratio_max.x
			if norm_ratio == INF: norm_ratio = 0
			var width: float = min_width + remaining_size.x * norm_ratio
			if c_header.maximum_size.x > 0 \
				and width > c_header.maximum_size.x:
					widths[j] = c_header.maximum_size.x
			else:
				widths[j] = width

			xoffset[j] = current_position.x

			# set the header vars
			c_header.position.x = xoffset[j]
			c_header.size.x = widths[j]

			# Update Accumulators
			current_position.x += widths[j]
			i += 1
			# get the next header
			c_header = c_headers[i] as Cell if i < c_headers.size() else null
		else:
			var norm_ratio: float = 1.0 / ratio_max.x
			if not norm_ratio: norm_ratio = 1 # Account for a divide by zero.
			var width: float = min_col_width + remaining_size.x / norm_ratio
			#widths[j] = width
			widths[j] = min_col_width
			xoffset[j] = current_position.x
			# Update Accumulator
			current_position.x += widths[j]

	# Loop through row headers to find height and y position
	var r_headers: Array[Node] = row_headers.get_children()
	r_headers.sort_custom( func(a,b) : return a.coord.y < b.coord.y )
	i = 0
	#Hide or show cells within the domain
	var r_header : Cell = r_headers[i] as Cell if i < r_headers.size() else null

	current_position = Vector2.ZERO
	for j in range( final_domain.size.y ):
		var y = final_domain.position.y + j
		if r_header and r_header.coord.y == y:
			# There exists a header at this x coord.
			r_header.position = current_position
			r_header.size.y = min_row_height
			heights[j] =  r_header.size.y
			yoffset[j] = current_position.y
			# Update Accumulators
			current_position.y += r_header.size.y
			i += 1
			# get the next header
			r_header = r_headers[i] as Cell if i < r_headers.size() else null
		else:
			heights[j] = min_row_height
			yoffset[j] = current_position.y
			current_position.y += min_row_height


func resize_cells() -> void:
	var i: int         =  0
	var interior_cells := cells.get_children().filter( is_cell_in_domain )

	# Sort those from top left to bottom right
	interior_cells.sort_custom( Cell.sort_position )

	var cell : Cell = interior_cells[i] as Cell if i < interior_cells.size() else null
	var current_position := Vector2.ZERO
	for j in range( final_domain.size.y ):
		var y = final_domain.position.y + j
		current_position.x = 0
		for k : int in range( final_domain.size.x ):
			var x: int = final_domain.position.x + k
			if cell and cell.coord == Vector2i(x,y):
				# There exists a cell at this x coord.
				cell.position = current_position
				cell.size = Vector2( widths[k], heights[j] )
				# get the next cell
				i += 1
				cell = interior_cells[i] as Cell if i < interior_cells.size() else null
			#Update position
			current_position.x += widths[k]
		current_position.y += heights[j]

#endregion


#region Table Level Operations
# Table Level Operations
# - clear()
# - sort()

func clear():
	var children: Array[Node] = cells.get_children()
	for index in range( children.size() ):
		children[index].name = "deleted"
		children[index].queue_free()

#endregion

# Selection Level Operations
# - erase/delete
# - Cut
# - Copy
# - Paste

# Row Level Operations
# - insert empty above
# - insert empty below


# Column Level Operations
# - insert before
# - insert after

# Cell level operations
# - ?

#region Query Operations
# - next_row() - get the next empty row
# - find row
# - find column
# - which column?
# 	- Which Column From Position
#	- Which Column From Text
#	- which Column from Callable that returns bool

func next_row() -> Vector2i:
	var this_range: Rect2i = get_range()
	return Vector2i(  this_range.position.x,  this_range.position.y +  this_range.size.y )

func find( match_func : Callable, region : Control = cells ) -> Array:
	return region.get_children().filter( match_func )

func find_first( match_func : Callable, region : Control = cells ) -> Cell:
	var cells: Array = region.get_children().filter( match_func )
	if cells.is_empty(): return null
	return cells.front()

func find_area( match_func : Callable, area : Rect2i, region : Control = cells) -> Array:
	return region.get_children().filter(
		func(cell : Cell):
			return area.has_point( cell.coord ) and match_func.call( cell ) )

#endregion

#region data Administration
# Get the domain headers in order, with missing headers as null
func get_domain_column_headers() -> Array[Header]:
	var final_headers : Array[Header] = []
	final_headers.resize( final_domain.size.x )

	var all_headers : Array = col_headers.get_children()

	var headers : Array = all_headers.filter( func( header : Header) :
		if final_domain.has_point( header.coord ): return true
		return false
		)

	if headers.size() == 0: return final_headers
	headers.sort_custom( func( a, b ): return a.coord.x < b.coord.x )

	var h : int = 0 # header index
	var header : Header

	for i in range( final_domain.size.x ):
		var x = final_domain.position.x + i
		header = headers[h] if h < headers.size() and headers[h].coord.x == x else null
		if header:
			final_headers[i] = header
			h += 1

	return final_headers

func get_domain_row_headers() -> Array:
	var final_headers : Array = []
	final_headers.resize( final_domain.size.y )

	var all_headers : Array = row_headers.get_children()

	var headers: Array = all_headers.filter( func( header : Header) :
		return true if final_domain.has_point( header.coord ) else false )

	if headers.size() == 0: return final_headers
	headers.sort_custom( func( a, b ): return a.coord.y < b.coord.y )
	var h : int = 0 # header index
	var header : Header
	for i in range( final_domain.size.y ):
		var y = final_domain.position.y + i
		header = headers[h] if headers[h].coord.y == y else null
		if header:
			final_headers[i] = header
			h += 1

	return final_headers

# Get Text
func get_cell_text( coord : Vector2i ) -> String:
	var cell : Cell = cells.get_node_or_null( "%s" % hash( coord ) )
	return cell.get_text() if cell.has_text() else ""

func get_cells_text( coord : Vector2i, size := Vector2.ONE ) -> Array[String]:
	var array : Array[String] = []
	cells.resize( size.x * size.y )
	for x in range( coord.x, size.x ):
		for y in range( coord.y, size.y ):
			var cell : Cell = cells.get_node_or_null( "%s" % hash( Vector2(x,y) ) )
			array[y * size.x + x] = cell.get_text() if cell.has_text() else ""
	return array


# Get Cells
func get_cell( coord : Vector2i, region : Control = cells ) -> Cell:
	return region.get_node_or_null( "%s" % hash( coord ) )

func is_cell_in_domain( cell : Cell ) -> bool:
	if final_domain.has_point( cell.coord ):
		return true
	return false

#region Write
# [data, options], position, region = cells

func write_cell( object, coord : Vector2i, region : Control = cells ) -> void:
	# I could create protected ranges which prevent overwrite,
	# but for now I will just delete an existing cell
	# is there an existing cell in that location?
	var cell : Cell = region.get_node_or_null( "%s" % hash( coord ) )
	if cell:
		cell.name = "deleted"
		cell.queue_free()

	if object is Cell:
		cell = object
		cell.coord = coord
		region.add_child( cell )
		cell.owner = get_tree().edited_scene_root
	elif object is Node:
		object.set_anchors_preset( LayoutPreset.PRESET_FULL_RECT )
		cell = create_cell( coord, region ) if region == cells else create_header( coord, region )
		cell.add_child( object )
		object.owner = get_tree().edited_scene_root
	else:
		var string : String = "%s" % object
		if string.is_empty(): return
		cell = create_cell( coord, region ) if region == cells else create_header( coord, region )
		var label: Label = add_label( cell, string )

	if not final_domain.has_point( coord ):
		dirty_domain = true
	if not  table_range.has_point( coord ):
		dirty_range = true
	resized.emit.call_deferred()


func write_dictionary( dict : Dictionary, position : Vector2i, region : Control = cells ):
	for key : String in dict:
		var header_cell: Cell = find_first( func(c:Cell): return c.get_text() == key, col_headers )
		if not header_cell:
			print( "Unable to match dictionary key to column header")
			continue
		var new_position := Vector2i( header_cell.coord.x, position.y )
		write_cell( dict[key], new_position )


func write_array( array : Array, position : Vector2i, region : Control = cells ):
	for i in range( array.size() ):
		write_cell( array[i], position + Vector2i(i,0), region )


func write_ml_array( data : Array, position : Vector2i, region : Control = cells ):
	var array : Array = data[0]
	var stride : int = data[1]
	for i in range( array.size() ):
		var x = position.x + (i % stride )
		var y = position.y + (i / stride )
		write_cell( array[i], Vector2( x, y ), region )

#endregion

# Insert
func insert_row( coord : Vector2i, objects : Array ):
	pass

func insert_column( coord : Vector2i, objects : Array ):
	pass

#Erase
func erase_cells( coord : Vector2i, size := Vector2.ONE, region : Control = cells ):
	for i in range( size.x ):
		var x = coord.x + i
		for j in range( size.y ):
			var y          = coord.y + j
			var cell: Node = region.get_node_or_null( "%s" % hash( Vector2(x,y) ) )
			if cell:
				cell.name = "deleted"
				cell.queue_free()


# Delete
func delete_row( coord : Vector2i ):
	printerr("delete_row( coord : %s ) is unimplemented" % coord )

func delete_column( coord : Vector2i ):
	printerr("delete_column( coord : %s ) is unimplemented" % coord )
#endregion

func create_header( coord : Vector2i, region : Control ) -> Header:
	var new_cell = PanelContainer.new()
	new_cell.set_script( header_script )
	new_cell.coord = coord
	region.add_child( new_cell )
	new_cell.owner = get_tree().edited_scene_root
	new_cell.resized.connect( _on_resized )
	return new_cell

func create_cell( coord : Vector2i, region : Control ) -> Cell:
	var new_cell = PanelContainer.new()
	new_cell.set_script( cell_script )
	new_cell.coord = coord
	region.add_child( new_cell )
	new_cell.owner = get_tree().edited_scene_root
	return new_cell

func add_label( cell : Cell, text : String ) -> Label:
	var new_label = Label.new()
	new_label.text = text
	# new_label_formatting
	new_label.vertical_alignment = VERTICAL_ALIGNMENT_CENTER
	new_label.horizontal_alignment = HORIZONTAL_ALIGNMENT_CENTER
	new_label.set_anchors_preset( LayoutPreset.PRESET_FULL_RECT )
	cell.add_child( new_label )
	new_label.owner = get_tree().edited_scene_root
	return new_label

# Miscelaneous stuff
func shrink_rect( rect : Rect2, margin : Vector4 = Vector4(1,1,1,1) ) -> Rect2:
	rect.position.x += margin.x
	rect.position.y += margin.y
	rect.size.x -= margin.x + margin.z
	rect.size.y -= margin.y + margin.w
	return rect
