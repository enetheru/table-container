@tool
extends EditorPlugin

# ready variables
const CellEditor = preload('res://addons/table_container/cell_editor.gd')
const TableTools = preload('res://addons/table_container/table_tools.gd')

@onready var cell_editor = preload('res://addons/table_container/cell_editor.tscn').instantiate()
@onready var tools = preload('res://addons/table_container/table_tools.tscn').instantiate()

# Configuration
# TODO get these from the editor config
var highlight_enabled := true

const COLOR_BASE := Color.WEB_GRAY * Color(1,1,1,0.2)
const COLOR_SELECTED := Color.SKY_BLUE * Color(1,1,1,0.5)
const COLOR_HOVER := Color.ROYAL_BLUE * Color(1,1,1,0.2)
const COLOR_ROW := Color.CORNFLOWER_BLUE * Color(1,1,1,0.1)
const COLOR_COLUMN := Color.CORNFLOWER_BLUE * Color(1,1,1,0.1)

# Active Variables
var table : Table

# Components
var table_rect := Rect2(0,0,1,1)

var header_col_rect := Rect2(0,0,1,1)
var header_row_rect := Rect2(0,0,1,1)

var cells_rect := Rect2(0,0,1,1)

#Mouse Hover
var hover_cell := Vector2i(0,-1)
var hover_cell_rect := Rect2(0,0,1,1)

var hover_column := Vector2i(0,-1)
var hover_column_rect := Rect2(0,0,1,1)

var hover_row := Vector2i(0,-1)
var hover_row_rect := Rect2(0,0,1,1)

#Selected
var selected_cell := Vector2i(0,-1)
var selected_cell_rect := Rect2(0,0,1,1)
var selected_region : Control

var selection := []

var selected_header_col := Vector2i(0,-1)
var selected_header_col_rect := Rect2( 0,0,1,1 )

var selected_header_row := Vector2i(0,-1)
var selected_header_row_rect := Rect2( 0,0,1,1 )

#Editing
var edited_region : Control
var edited_cell := Vector2i(0,-1)
var edited_cell_rect := Rect2(0,0,1,1)

# Last Input Events
var last_mm : InputEventMouseMotion
var last_mb : InputEventMouseButton

# Local to canvas, and vice versa
var l2c : Transform2D
var c2l : Transform2D

func _ready() -> void:
	cell_editor.plugin = self
	tools.plugin = self
	tools.hide()
	#add_control_to_container( EditorPlugin.CONTAINER_CANVAS_EDITOR_MENU, toolbar)
	add_control_to_container( EditorPlugin.CONTAINER_CANVAS_EDITOR_SIDE_LEFT, tools)


func _handles( object: Object ) -> bool: return object is Table

func _edit( object : Object ) -> void:
	if object == table: return
	reset()
	if not object:
		close()
		return

	# update cached variables.
	table = object
	selected_region = table.cells
	edited_region = table.cells
	print( "connecting to table.resize signal" )
	table.resized.connect( update_overlays )
	table.resized.connect( update_table_rects )
	update_table_rects()
	# Update tools to point to this table
	tools.show()
	cell_editor.plugin  = self
	cell_editor.table = object

func reset():
	print( "table plugin resetting")
	if cell_editor.visible: cell_editor.close()
	if table:
		print( "disconnecting from table.resized")
		table.resized.disconnect( update_overlays )
		table.resized.disconnect( update_table_rects )
	table = null

func close():
	print( "table plugin closing")
	tools.hide()


# This typically only runs when:
# - the viewport goes in and our of focus
# - when the object has changed in some fashion.
# But I make it run  when any mouse event occurs inside the control
func _forward_canvas_draw_over_viewport(viewport_control: Control) -> void:
	if not table.editor_controls: return

#region Get the transforms from the viewport and the table.
	var transform: Transform2D = viewport_control.get_canvas_transform()

	# this transform translates from local position to canvas/viewport position
	# This is the one to use when drawing
	l2c = table.get_viewport_transform() * table.get_global_transform()

	# This translates from canvas/viewport mouse position, to local position.
	# This is the one to use when picking cells using mouse coordinates.
	c2l = table.get_global_transform().affine_inverse() * table.get_viewport_transform().affine_inverse()
#endregion

	# HACK I dont this this is the best location for this code,
	# but I dont kow where else to put it/
	if not cell_editor.get_parent():
		viewport_control.add_child( cell_editor )

	# Make sure ethe table_rect is upto date.
	update_table_rects()

	# transformations and property changes of the table object can effect
	# the positions of objects in our overlay, so we need to calculate them
	# again when we draw, just incase.
	hover_column_rect = table.get_column_rect( hover_column )
	hover_row_rect = table.get_row_rect( hover_row )
	hover_cell_rect = table.get_cell_rect( hover_cell )
	hover_cell_rect.position += table.cells.position

	var edited_rect: Rect2 = table.get_cell_rect( edited_cell )
	edited_rect.position += edited_region.position
	copy_transform( cell_editor, edited_rect )

	if selected_region == table.cells:
		selected_cell_rect = table.get_cell_rect( selected_cell  )
	elif selected_region == table.row_headers:
		selected_cell_rect = table.get_row_heading_rect( selected_cell )
	elif selected_region == table.col_headers:
		selected_cell_rect = table.get_col_heading_rect( selected_cell )
	selected_cell_rect.position += selected_region.position

#region Begin drawing in control local space
	viewport_control.draw_set_transform_matrix(l2c) # we are drawing ontop of the table

	# TODO Draw the heading background.
	# TODO Draw the numbers background.

	# drawing the table interior has to be in order from back to front.
	# - background
	# - column highlight
	# - row highlight
	# - cell highlight
	# - selected cell

	## Draw column, row, and hover highlights
	if hover_column.y >= 0:
		viewport_control.draw_rect( hover_column_rect, COLOR_COLUMN )

	if hover_row.y >= 0:
		viewport_control.draw_rect( hover_row_rect, COLOR_ROW )

	if hover_cell.y >= 0:
		viewport_control.draw_rect( hover_cell_rect, COLOR_HOVER )

	## Draw selected highlight
	if selected_cell.y >= 0:
		viewport_control.draw_rect( selected_cell_rect, COLOR_SELECTED, false, 3 )

	viewport_control.draw_set_transform_matrix(transform) # reset the matrix
#endregion End drawing in control local space


func _forward_canvas_gui_input(event: InputEvent) -> bool:
	if not table.editor_controls: return false

#region Mouse Motion
	if event is InputEventMouseMotion:
		last_mm = event as InputEventMouseMotion
		# Translate position of mouse to control local space
		var table_position: Vector2 = c2l * last_mm.position

		# if the mouse position is not inside the control, then dont bother.
		if not table_rect.has_point( table_position ):
			hover_cell = Vector2i(0,-1)
			hover_column = Vector2i(0,-1)
			hover_row = Vector2i(0,-1)
			update_overlays()
			return false

		# Get the coordinates of the cell that the mouse cursor is over.
		if table.show_col_header and header_col_rect.has_point( table_position ):
			hover_row = Vector2i( 0,-1 )
			hover_cell = Vector2i( 0,-1 )
			var header_position: Vector2 = table_position - header_col_rect.position
			var this_column: Vector2i    = table.pos2coord( header_position )
			if hover_column != this_column:
				hover_column = this_column

		if table.show_row_header and header_row_rect.has_point( table_position ):
			hover_column = Vector2i( 0,-1 )
			hover_cell = Vector2i( 0,-1 )
			var this_row: Vector2i = table.pos2coord( table_position - header_row_rect.position )
			if hover_row != this_row:
				hover_row = this_row

		if cells_rect.has_point( table_position ):
			var cells_position: Vector2 = table_position - cells_rect.position
			var this_cell: Vector2i     = table.pos2coord( cells_position )
			if hover_cell != this_cell:
				hover_cell = this_cell
				hover_column = this_cell
				hover_row = this_cell

		update_overlays()
		return false
#endregion
#region MouseButton
	if event is InputEventMouseButton:
		last_mb = event as InputEventMouseButton
		# Translate position of mouse to control local space
		var table_position: Vector2 = c2l * last_mb.position

		# if the mouse position is not inside the control, then dont bother.
		if not table_rect.has_point( table_position ):
			selected_cell = Vector2i(0,-1)
			if cell_editor.visible: cell_editor.close()
			return false

		var this_cell : Vector2i
		var this_region : Control
		# Alter the behaviour depending on where in the table is clicked
		if table.col_headers.get_rect().has_point( table_position ):
			this_region = table.col_headers
			this_cell = table.pos2coord( table_position - table.col_headers.position )
		elif table.row_headers.get_rect().has_point( table_position ):
			this_region = table.row_headers
			this_cell = table.pos2coord( table_position - table.row_headers.position )
		elif table.cells.get_rect().has_point( table_position ):
			this_region = table.cells
			this_cell = table.pos2coord( table_position - table.cells.position )

		else:
			return false

		# TODO I want right click to open the add_node dialog.
		# TODO Multi Cell Select
		if last_mb.pressed:
			if [this_cell, this_region] != [selected_cell, selected_region]:
				if cell_editor.visible: cell_editor.close()
				selected_cell = this_cell
				selected_region = this_region
				print( selected_region, selected_cell )
				update_overlays()

		if last_mb.double_click:
			edited_region = this_region
			edited_cell = this_cell
			cell_editor.edit( this_region, this_cell )
			update_overlays()

		# Consume the mouse button event
		return true
#endregion

#region Keyboard Input Handling
	if event is InputEventKey:
		var matched : bool = true
		if event.keycode >= 32 and event.keycode <=167:
			if event.is_command_or_control_pressed():
				return false
			if event.pressed:
				edited_region = selected_region
				edited_cell = selected_cell
				cell_editor.edit( edited_region, edited_cell )
				#NOTE: I honestly hate that I have to do something like this
				# but I cannot find any way to get the lowercase key from the key event
				var ks : String = OS.get_keycode_string( event.keycode )
				if event.shift_pressed:
					cell_editor.insert_text_at_caret( ks )
				else:
					cell_editor.insert_text_at_caret( ks.to_lower() )
				update_overlays()
				return matched
		match [event.keycode, event.pressed]:
			[KEY_DELETE, true]:
				table.erase_cells( selected_cell, Vector2i.ONE, selected_region )
			[KEY_BACKSPACE, true]:
				table.erase_cells( selected_cell )
			[KEY_ENTER, true]:
				# Commit changes to cell and move down/up one
				if event.shift_pressed: selected_cell += Vector2i(0,-1)
				else: selected_cell += Vector2i(0,1)
			[KEY_TAB, true]:
				# Commit changes to cell and move right/left one
				if event.shift_pressed: selected_cell += Vector2i(-1,0)
				else: selected_cell += Vector2i(1,0)
			[KEY_F2, true]:
				edited_cell = selected_cell
				edited_region = selected_region
				cell_editor.edit( selected_region, selected_cell )
			# up down left and right
			[KEY_UP, true]:
				if selected_region == table.col_headers: pass
				else: selected_cell += Vector2i(0,-1 if selected_cell.y > 0 else 0)
			[KEY_DOWN, true]:
				if selected_region == table.col_headers: pass
				else: selected_cell += Vector2i(0,1)
			[KEY_LEFT, true]:
				if selected_region == table.row_headers: pass
				else: selected_cell += Vector2i(-1,0)
			[KEY_RIGHT, true]:
				if selected_region == table.row_headers: pass
				else: selected_cell += Vector2i(1,0)
			_: matched = false
		update_overlays()
		return matched
#endregion
	# If no event has handled, we need to pass it back to the editor
	return false

#region Utility Functions

func update_table_rects():
	table_rect.position = Vector2(0,0)
	table_rect.size = table.size

	header_col_rect = table.col_headers.get_rect()
	header_row_rect = table.row_headers.get_rect()
	cells_rect = table.cells.get_rect()

func copy_transform( control : Control, sub_rect : Rect2 ):
	control.position =  l2c * sub_rect.position
	control.size = sub_rect.size
	control.rotation = l2c.get_rotation()
	control.scale = l2c.get_scale()

#endregion

func get_selected_cell() -> Cell:
	return table.get_cell( selected_cell, selected_region )
