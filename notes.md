My own flavour of table container.

I want to be able to specify the columns using a dictionary of items.

The dictionary will specify the weights of the container, so that the widths
can be calculated before the items are layed out.

I want:
```gdscript

enum {
    LAYOUT_FIXED, # The width in pixels
    LAYOUT_RATIO, # Interprets width as a 0.0 -> 1.0 ratio value based on the parent container size
    LAYOUT_AUTO   # width is ratio of all column widths combined sort of.
}

class column_layout:
    var width : int 10 # float value for width
    var mode : int = LAYOUT_AUTO # the way the width is calculated
    var heading : String = "heading" #heading text


# Container API
func get_cell_hr( heading : String, row : int ) -> Node : pass
func get_cell_xy( coord : Vector2 ) -> Node: pass
func get_row( row : int ) -> Array : pass
func get_column( column : int ) -> Array: pass

#And respective set functions

```

Anyway it's freezing, so I am leaving.
